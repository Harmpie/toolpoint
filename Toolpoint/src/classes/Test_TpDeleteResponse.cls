@isTest
public class Test_TpDeleteResponse {
    //
    // Local insert responses are tested through TpOrg
    private static testMethod void testRemoteParser() {
        String xmlDummy = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com"><soapenv:Body><deleteResponse><result><id>003D000000OXWBgIAP</id><success>true</success></result><result><id>003D000000OXWBhIAP</id><success>true</success></result></deleteResponse></soapenv:Body></soapenv:Envelope>';
		Dom.Document doc = new Dom.Document();
        doc.load(xmlDummy);
        Test.startTest();
        TpDeleteResponse resp = new TpDeleteResponse(doc);
        System.assertEquals(2,resp.numResults,'Result counter should be 2');
        System.assertEquals(2,resp.resultRows.size(),'Result rows should have 2 elements');
        Test.stopTest();
    }
}