@isTest
private class Test_TpOrgInterface {
    private static testMethod void testOrgItf() {
        //
        // Testing locally. Assuming Account/Contact is available
        Account a = new Account(Name='TEST APEX ACCOUNT');
        insert a;
        Contact c = new Contact(LastName='TEST APEX CONTACT',AccountId=a.Id);
        insert c;
        Test.startTest();
        TpOrgInterface itf = new TpOrgInterface();
        //
        // To avoid having to deal with callouts, use 2x local org here
        itf.sourceOrg.isLocal = true;
        itf.targetOrg.isLocal = true;
        itf.sourceOrg.resetOrgConnection();
        itf.targetOrg.resetOrgConnection();

        System.assertEquals(0,itf.interfaces.size(), 'TpSobjectInterfaces should be empty');
        System.assertEquals(null,itf.sourceOrg.userName, 'Source Username should be empty');
        System.assertEquals(null,itf.targetOrg.userName, 'Target Username should be empty');
        System.assertEquals(0,itf.cacheSize, 'Id Cache should be empty');
        System.assertEquals(false,itf.orgsDifferent, 'Orgs should not be different! ');
        
        System.assert(itf.sObjectOptions.size()>0, 'Expect at least some objects on these local orgs');
        System.assertEquals(0,itf.selectedSObjects.size(), 'Expected empty selectedSObjects');
        
        //
        // Let's assume there's an Account/Contact SObject  
        String testWithSobject = 'account'; 
        String testWithSobject2 = 'contact';
        System.assert(itf.getCommonFieldList(testWithSobject).size()>0, 'We should have common fields!');
        System.assert(itf.getWritableCommonFieldList(testWithSobject).size()>0, 'We should have common writeable fields!');

        //
        // Init interfaces
        itf.interfaces = null;
        itf.selectedSObjects.add(testWithSobject); 
        itf.selectedSObjects.add(testWithSobject2); 
        
        System.assertEquals(2,itf.interfaces.size(), 'TpSobjectInterfaces should have 2 element');
        
        for(TpSobjectInterface soi : itf.interfaces) {
            System.assert(soi.query.toLowerCase().contains('from contact') || soi.query.toLowerCase().contains('from account'),'Invalid query: '+soi.query);             
            if(soi.sobjectName.toLowerCase() == 'account') {
                soi.executionOrder = 1;
            }
            if(soi.sobjectName.toLowerCase() == 'contact') {
                soi.executionOrder = 2;
            }
        }
        
        itf.executeAll();
        //
        // Should contain 1 contact id and 1 account id
        System.assertEquals(2,itf.cacheSize, 'Id Cache should have 2 elements');
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Resulted in') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        
        itf.testAll();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Resulted in') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        itf.clearCache();
        System.assertEquals(0,itf.cacheSize, 'Id Cache should be empty');
        
        Test.stopTest();
    }
}