//
// Handles the results of a query and converts to TpSobjects. 
// Results are place in class variable sObjectRows
public class TpQueryResult {
    // -------------------------------------------------------------
    // Class variables
    // -------------------------------------------------------------
    public Integer numRows;
    public String queryLocator;
    public Boolean isDone;
    public List<TpSobject> sObjectRows {
        get { 
            if(sObjectRows == null) {
                sObjectRows = new List<TpSobject>();
            }
            return sObjectRows;
        }
        set;
    }
    private TpOrg theOrg; // Only relevant in case of XML parsing
    private String sobjectName;
    // -------------------------------------------------------------
    // Constructors
    // ------------------------------------------------------------- 
    public TpQueryResult(List<SObject> sobjects) {
		parse(sobjects);        
    }
    public TpQueryResult(Dom.Document xmlDoc, TpOrg org) {
        this.theOrg = org;
        parse(xmlDoc);
    }
    //
    // Only for testing
    public TpQueryResult() {
        
    }
    //
    // Querymore should call this
    public void appendRows(Dom.Document xmlDoc) {
    	parse(xmlDoc);    
    }
    // -------------------------------------------------------------
    // Private methods
    // ------------------------------------------------------------- 
    private void parse(List<SObject> sobjects) {
        if(sobjects == null || sobjects.isEmpty()) {
            throw new TpQueryResultException('No sobjects, nothing to parse!');
        }
        System.Debug('### Parsing sobjects');
        
        //
        // Here we can work on the local schema, since 
        // only the local system can have returned actual SObjects        
        SObject soForType = sobjects[0];
        Schema.SObjectType sot = soForType.getSObjectType();
        Schema.DescribeSObjectResult sor = sot.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sor.fields.getMap();
		this.sobjectName = sor.getName();
        
        if(String.isEmpty(this.sobjectName)) {
        	throw new TpQueryResultException('Could not determine SObject name');    
        }
        if(fieldMap == null || fieldMap.isEmpty()) {
        	throw new TpQueryResultException('Could not determine SObject fields');    
        }
        
        //
        // All good, let's create the TpSobjects        
        for(SObject s : sobjects) {
        	TpSobject sObj = new TpSobject(this.sobjectName); 
            for(String fieldName : fieldMap.keySet()) {
                setFieldValue(sObj, s, fieldName.toLowerCase());
            }
            //
            // Add to list
            this.sObjectRows.add(sObj);
        }
    }
    private void parse(Dom.Document xmlDoc) {
        System.Debug('### Parsing XML File');
        if(this.theOrg == null || xmlDoc == null) {
            throw new TpQueryResultException('Org and xmlDoc are required to parse XML!');
        }
        Dom.XmlNode rootNode = xmlDoc.getRootElement();
        for(Dom.XMLNode child : rootNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'body') {
                handleBody(child);
            }
        }        
    }
    private void handleBody(Dom.XMLNode bodyNode) {
        System.Debug('### Body '+bodyNode.getName());
        for(Dom.XMLNode child : bodyNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'queryresponse') {
                handleQueryResponse(child);
            }
            if(child.getName().toLowerCase() == 'querymoreresponse') {
                handleQueryResponse(child);
            }
        }  
    }
    private void handleQueryResponse(Dom.XMLNode qrNode) {
        System.Debug('### Query response '+qrNode.getName());
        for(Dom.XMLNode child : qrNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'result') {
                handleResult(child);
            }
        }  
    }
    private void handleResult(Dom.XMLNode resultNode) {
        System.Debug('### Result '+resultNode.getName());
        for(Dom.XMLNode child : resultNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'records') {
                handleRecords(child);
            }
            if(child.getName().toLowerCase() == 'size') {
                this.numRows = Integer.valueOf(child.getText());
            }
            if(child.getName().toLowerCase() == 'done') {
                this.isDone = Boolean.valueOf(child.getText());
            }
            if(child.getName().toLowerCase() == 'querylocator') {
                this.queryLocator = String.valueOf(child.getText());
            }
        }  
    }
    //
    // Creates the actual TpSobjects
    // This method will execute once for each record in the results. Despite the nodename's plural form!
    private void handleRecords(Dom.XMLNode recordsNode) {
        System.Debug('### Records '+recordsNode.getName());
        //
        // First try and determine the type of SObject
        // This method can only handle SObjects of the same type
        // Store some details in class variables so we dont have to redo
        // this for each record in the list
        if(this.sobjectName==null) {
            for(Dom.XMLNode child : recordsNode.getChildElements()) {
                if(child.getName().toLowerCase() == 'type') {
                    if(this.theOrg.orgSobjects.containsKey(child.getText().toLowerCase())) {
                        this.sobjectName = child.getText();
                    } else {
                        throw new TpQueryResultException('Somehow the SObject you queried is unknown in the org you queried it from! This really should not have happened!');
                    }        
                    break;
                }
            }  
        } 
        //
        // Can't continue without the objectName
        if(this.sobjectName == null) {
        	throw new TpQueryResultException('An error occurred while trying to get org field information');
        }
        
        //
        // Construct the TpSobject
        TpSobject sObj = new TpSobject(this.sobjectName);
        
        //
        // Iterate through fields
        for(Dom.XMLNode child : recordsNode.getChildElements()) {
            
            setFieldValue(sObj, child);
        }  
        
        //
        // Add to list
        this.sObjectRows.add(sObj);
    }
    private void setFieldValue(TpSobject s, Dom.XMLNode node) {
		String nodeName = node.getName();
        String nodeValue = node.getText();
        if(String.isEmpty(nodeValue)) {
            return;
        } else {
            s.fieldValueMap.put(nodeName.toLowerCase(), nodeValue);
        }
    }
    private void setFieldValue(TpSobject tpSo, SObject s, String fieldName) {
        try {
            // 
            // Test presence of field on Sobject using try / catch
           	s.get(fieldName); 
        }
        catch(Exception e) {            
            //
            // Not present
            return;
        }
        String objValue = String.valueOf(s.get(fieldName));
        if(String.isEmpty(objValue)) {
            return;
        } else {
            tpSo.fieldValueMap.put(fieldName, objValue);
        }
    }

  	// -------------------------------------------------------
   	// Custom exception
   	// -------------------------------------------------------
   	public class TpQueryResultException extends Exception {
        
    }
}