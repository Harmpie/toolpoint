public class TpXml {
	public static String getQueryXml(String query, String sessionId) {
    	String queryXml = '<?xml version="1.0" encoding="UTF-8"?>\n';
    	queryXml += '<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n';
        queryXml += '<env:Header>\n';
        queryXml += '<SessionHeader xmlns="urn:partner.soap.sforce.com">\n';
    	queryXml += '<sessionId>'+sessionId+'</sessionId>\n';
    	queryXml += '</SessionHeader>\n';
    	queryXml += '</env:Header>\n';
        queryXml += '<env:Body>';
        queryXml += '<query xmlns="urn:partner.soap.sforce.com">';
        queryXml += '<queryString>'+query+'</queryString>';
        queryXml += '</query>';
        queryXml += '</env:Body>';
        queryXml += '</env:Envelope>';
        return queryXml;
    }
	public static String getQueryMoreXml(String queryLocator, String sessionId) {
    	String queryXml = '<?xml version="1.0" encoding="UTF-8"?>\n';
    	queryXml += '<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n';
        queryXml += '<env:Header>\n';
        queryXml += '<SessionHeader xmlns="urn:partner.soap.sforce.com">\n';
    	queryXml += '<sessionId>'+sessionId+'</sessionId>\n';
    	queryXml += '</SessionHeader>\n';
    	queryXml += '</env:Header>\n';
        queryXml += '<env:Body>';
        queryXml += '<queryMore xmlns="urn:partner.soap.sforce.com">';
        queryXml += '<queryLocator>'+queryLocator+'</queryLocator>';
        queryXml += '</queryMore>';
        queryXml += '</env:Body>';
        queryXml += '</env:Envelope>';
        return queryXml;
    }    
	public static String getInsertXml(String sessionId, String sobjectsXml) {
   		String insertXml = '<?xml version="1.0" encoding="UTF-8"?>\n';
        insertXml += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">\n';
        insertXml += '<soapenv:Header>\n';
        insertXml += '<urn:SessionHeader>\n';
    	insertXml += '<urn:sessionId>'+sessionId+'</urn:sessionId>\n';
	    insertXml += '</urn:SessionHeader>\n';
	    insertXml += '</soapenv:Header>\n';
        insertXml += '<soapenv:Body>\n';
        insertXml += '<urn:create xmlns="urn:partner.soap.sforce.com">\n';
        insertXml += sobjectsXml;
        insertXml += '</urn:create>\n';
        insertXml += '</soapenv:Body>\n';
        insertXml += '</soapenv:Envelope>\n';
  
        return insertXml;
    }
	public static String getDeleteXml(String sessionId, Set<String> sobjectIds) {
   		String deleteXml = '<?xml version="1.0" encoding="UTF-8"?>\n';
        deleteXml += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">\n';
        deleteXml += '<soapenv:Header>\n';
        deleteXml += '<urn:SessionHeader>\n';
    	deleteXml += '<urn:sessionId>'+sessionId+'</urn:sessionId>\n';
	    deleteXml += '</urn:SessionHeader>\n';
	    deleteXml += '</soapenv:Header>\n';
        deleteXml += '<soapenv:Body>\n';
        deleteXml += '<urn:delete xmlns="urn:partner.soap.sforce.com">\n';
        for(String idVal : sobjectIds) {
        	deleteXml += '<urn:ids>'+idVal+'</urn:ids>';
    	}
        deleteXml += '</urn:delete>\n';
        deleteXml += '</soapenv:Body>\n';
        deleteXml += '</soapenv:Envelope>\n';
  
        return deleteXml;
    }    
}