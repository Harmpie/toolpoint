@isTest
private class Test_DataPointWizard {
    private static testMethod void testWizard() {
        Test.startTest();
        PageReference pageRef = Page.DataPointWizard;
        Test.setCurrentPage(pageRef);
        DataPointWizardCtrl controller = new DataPointWizardCtrl();
        TpOrgInterface intf = new TpOrgInterface();
        controller.itf = intf; 
        //
        // Test double local
        controller.itf.sourceOrg.isLocal = true;
        controller.itf.targetOrg.isLocal = true;
        controller.itf.sourceOrg.resetOrgConnection();
        controller.itf.targetOrg.resetOrgConnection();

        System.assertEquals(Page.DataPointSelectSobjects.getUrl(), controller.refreshSobs().getUrl(),'Expected to end up in sobject selection');
        PageReference r = controller.defineDataSet();
        System.assertEquals(null, r ,'Expected to stay in sobject selection');
        
        controller.itf.selectedSObjects.add('account');
        System.assertEquals(Page.DataPointInterfaces.getUrl(), controller.defineDataSet().getUrl(),'Expected to end up in interface definition');
        //
        // Move back and forth
        System.assertEquals(Page.DataPointWizard.getUrl(), controller.configureOrgInterface().getUrl(),'Expected to end up in org config');
        System.assertEquals(Page.DataPointInterfaces.getUrl(), controller.defineDataSet().getUrl(),'Expected to end up in interface definition');
		
		//
		// Can't (well, could but not gonna do it) mimic the callout exception as it should be
		controller.getCoverage();                
        Test.stopTest();
    }
}