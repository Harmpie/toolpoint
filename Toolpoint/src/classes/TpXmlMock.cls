global class TpXmlMock {
    public static String getLoginRequestXml() {
        String output = '<?xml version="1.0" encoding="UTF-8"?><env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n';
        output += '<env:Header />\n'; 
        output += '<env:Body>\n'; 
        output += '<login xmlns="urn:partner.soap.sforce.com">\n'; 
        output += '<username>test@toolpoint.org</username>\n'; 
        output += '<password>secret</password>\n'; 
        output += '</login>\n'; 
        output += '</env:Body>\n'; 
		output += '</env:Envelope>\n'; 
        return output;
    } 
    //
    // Returns 2 bogus accounts. 
    // TODO: Must be made more dynamic
    public static String getQueryResponseXml() {
        String output = '<?xml version="1.0" encoding="UTF-8"?>';
        output += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sf="urn:sobject.partner.soap.sforce.com">';
		output += '<soapenv:Header>';
        output += '<LimitInfoHeader>';
        output += '<limitInfo>';
        output += '<current>214</current>';
        output += '<limit>15000</limit>';
        output += '<type>API REQUESTS</type>';
        output += '</limitInfo>';
        output += '</LimitInfoHeader>';
        output += '</soapenv:Header>';
        output += '<soapenv:Body>';
        output += '<queryResponse>';
		output += '<result xsi:type="QueryResult">';
        output += '<done>true</done>';
		output += '<queryLocator xsi:nil="true"/>';
        output += '<records xsi:type="sf:sObject">';
		output += '<sf:type>Account</sf:type>';
        output += '<sf:Id>001b000000CB9soAAD</sf:Id>';
		output += '<sf:Id>001b000000CB9soAAD</sf:Id>';
        output += '<sf:Name>APEX TEST ACCOUNT 2</sf:Name>';
		output += '<sf:Type>Customer - Channel</sf:Type>';
		output += '<sf:ParentId xsi:nil="true"/>';
		output += '<sf:BillingStreet>345 Shoreline Park Mountain View, CA 94043 USA</sf:BillingStreet>';
        output += '<sf:BillingCity>Mountain View</sf:BillingCity>';
        output += '<sf:BillingState>CA</sf:BillingState>';
        output += '<sf:BillingPostalCode xsi:nil="true"/>';
        output += '<sf:BillingCountry xsi:nil="true"/>';
        output += '<sf:BillingLatitude xsi:nil="true"/>';
        output += '<sf:BillingLongitude xsi:nil="true"/>';
        output += '<sf:ShippingStreet>345 Shoreline Park Mountain View, CA 94043 USA</sf:ShippingStreet>';
        output += '<sf:ShippingCity xsi:nil="true"/>';
        output += '<sf:ShippingState xsi:nil="true"/>';
        output += '<sf:ShippingPostalCode xsi:nil="true"/>';
        output += '<sf:ShippingCountry xsi:nil="true"/>';
        output += '<sf:ShippingLatitude xsi:nil="true"/>';
        output += '<sf:ShippingLongitude xsi:nil="true"/>';
		output += '<sf:Phone>(650) 867-3450</sf:Phone>';
        output += '<sf:Fax>(650)867-9895</sf:Fax>';
		output += '</records>';
        output += '<records xsi:type="sf:sObject">';
		output += '<sf:type>Account</sf:type>';
        output += '<sf:Id>001b000000CB9soAAD</sf:Id>';
		output += '<sf:Id>001b000000CB9soAAD</sf:Id>';
        output += '<sf:Name>APEX TEST ACCOUNT 2</sf:Name>';
		output += '<sf:Type>Customer - Channel</sf:Type>';
		output += '<sf:ParentId xsi:nil="true"/>';
		output += '<sf:BillingStreet>345 Shoreline Park Mountain View, CA 94043 USA</sf:BillingStreet>';
        output += '<sf:BillingCity>Mountain View</sf:BillingCity>';
        output += '<sf:BillingState>CA</sf:BillingState>';
        output += '<sf:BillingPostalCode xsi:nil="true"/>';
        output += '<sf:BillingCountry xsi:nil="true"/>';
        output += '<sf:BillingLatitude xsi:nil="true"/>';
        output += '<sf:BillingLongitude xsi:nil="true"/>';
        output += '<sf:ShippingStreet>345 Shoreline Park Mountain View, CA 94043 USA</sf:ShippingStreet>';
        output += '<sf:ShippingCity xsi:nil="true"/>';
        output += '<sf:ShippingState xsi:nil="true"/>';
        output += '<sf:ShippingPostalCode xsi:nil="true"/>';
        output += '<sf:ShippingCountry xsi:nil="true"/>';
        output += '<sf:ShippingLatitude xsi:nil="true"/>';
        output += '<sf:ShippingLongitude xsi:nil="true"/>';
		output += '<sf:Phone>(650) 867-3450</sf:Phone>';
        output += '<sf:Fax>(650)867-9895</sf:Fax>';
		output += '</records>';
        output += '<size>2</size>';
        output += '</result>';
        output += '</queryResponse>';
        output += '</soapenv:Body>';
        output += '</soapenv:Envelope>';
    	return output;
    } 
    
}