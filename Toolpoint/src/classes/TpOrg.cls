/*
https://login.salesforce.com/services/Soap/u/33.0
https://test.salesforce.com/services/Soap/u/33.0      
*/
public class TpOrg {
    // -------------------------------------------------------
    // Class variable
    // -------------------------------------------------------
    public Boolean isLocal { get; set; }
    public String loginEndpoint { get; set; }
    public String userName { get; set; }
    public String password { get; set; }
    public String securityToken { get; set; }
    public String loggedinUserName { get; private set; }
    public String serverUrl { get; private set; }
    public String sessionId { get; private set; } 
    public String orgName { get; private set; } 
    public String orgId { get; private set; } 
    //
    // This field is set on each callout exception
    // Can be used to for example detect a missing Remote Site setting
    public Boolean calloutError { get; private set; }
    private DateTime connectionTime { get; private set; }
    private DateTime sessionEnds { get; private set; }
    public Map<String, String> idCache { 
        get {
            if(idCache == null) {
                idCache = new Map<String, String>();
            }
            return idCache;
        }  
        private set; 
    }
    // -------------------------------------------------------
    // Constructors
    // -------------------------------------------------------
    public TpOrg() {
        initLocalOrg();
    }
    public TpOrg(String un, String pw, String st, String url) {
        this.calloutError = false;
        this.isLocal = false;   
        this.userName = un;        
        this.password = pw;
        this.securityToken = st;
        this.loginEndpoint = url;
        this.orgId = null;
        this.orgName = null;
        this.loggedinUserName = null;
        this.sessionEnds = null;
        this.connectionTime = null;
    }

    // -------------------------------------------------------
    // Public methods
    // -------------------------------------------------------
    //
    // Resets org
    public void resetOrgConnection() {
        //
        // Reset and try to connect
        this.sessionId = null;
        this.serverUrl = null;
        this.orgSobjects = null;
        if(this.isLocal) {
            initLocalOrg();
        } else {
            connect();
            System.assert(this.orgSobjects.size() > 0);
        }
    }
    public void clearCache() {
        this.idCache = null;
    }
    // Fetches a map of objectinfo indexed by lowercase objectname
    public Map<String,TpPartnerConnection.DescribeGlobalSObjectResult> orgSobjects { 
        get {
            if(orgSobjects == null) {
                orgSobjects = new Map<String,TpPartnerConnection.DescribeGlobalSObjectResult>();
                List<TpPartnerConnection.DescribeGlobalSObjectResult> results;
                if(this.isLocal) {
                    results = this.getLocalSobjectResults();
                } else {
                    results = this.getRemoteSobjectResults();
                }
                //
                // Now put all in a map
                for(TpPartnerConnection.DescribeGlobalSObjectResult res : results) {
                	orgSobjects.put(res.name.toLowerCase(), res);    
                }
            }
            return orgSobjects;
        }
        set; 
    }
    
    //
    // Set of all object fields + info indexed by lowercase field name
    public Map<String, TpPartnerConnection.Field> getObjectFieldInfos(String sObjectName) {
        Map<String, TpPartnerConnection.Field> output = new Map<String, TpPartnerConnection.Field>();
        List<TpPartnerConnection.Field> results;
        if(this.isLocal) {
			results = this.getLocalFieldInfos(sObjectName);
        } else {
            if(!Test.isRunningTest()) {
	            results = this.getRemoteFieldInfos(sObjectName);
            }
        }
        if(results <> null) {
            for(TpPartnerConnection.Field tpf : results) {
                //output.put(tpf.name, tpf);    
                output.put(tpf.name.toLowerCase(), tpf);    
            }
        }
        return output;
    }
    
    //
    // Executes a string SOQL query
    public List<TpSobject> executeQuery(String theQuery) {
        if(String.isEmpty(theQuery)) {
			throw new TpOrgException('No query!');
        }
        
		List<TpSobject> output = new List<TpSobject>();
        TpQueryResult tpRes;
        if(this.isLocal) {
			tpRes = this.doLocalQuery(theQuery);    
        } else {
            tpRes = this.doRemoteQuery(theQuery);
        }
        output = tpRes.sObjectRows;
        return output;
    }
    
    //
    // Executes an insert on the TpSobjects
    public List<TpSobjectResult> executeInsert(List<TpSobject> sobs) {
        if(sobs == null) {
			throw new TpOrgException('No sobjects!');
        }
        List<TpSobjectResult> results;
        TpInsertResponse resp;
        if(this.isLocal) {
        	resp = this.insertLocal(sobs);    
        } else {
            if(!Test.isRunningTest()) {
	        	resp = this.insertRemote(sobs);
            }
        }   
        if(resp<>null) {
        	results = resp.resultRows;
        }
        return results;
    }
    //
    // Executes an insert on the TpSobjects
    public List<TpSobjectResult> executeDelete(List<TpSobject> sobs) {
        if(sobs == null) {
			throw new TpOrgException('No sobjects!');
        }
        List<TpSobjectResult> results;
        TpDeleteResponse resp;
        if(this.isLocal) {
        	resp = this.deleteLocal(sobs);    
        } else {
			if(!Test.isRunningTest()) {
				resp = this.deleteRemote(sobs);
            }
        }   
        if(resp<>null) {
	        results = resp.resultRows;
        }
        System.Debug('### Returning delete '+resp);
        return results;
    }
    
    // -------------------------------------------------------
    // Private methods
    // ------------------------------------------------------- 

    // -------------------------------------------------------
    // Operations
    // ------------------------------------------------------- 
    // 
    //  Executes the query 
    private TpQueryResult doLocalQuery(String query) {
        System.Debug('### Local query: '+query);
       	TpQueryResult tpRes = new TpQueryResult(Database.query(query));
        return tpRes;
    }      
    private TpQueryResult doRemoteQuery(String query) {
        System.Debug('### Remote query: '+query);
        this.connect();
        String theXml = TpXml.getQueryXml(query, this.sessionId);
        Http conn = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(this.serverUrl);
        req.setMethod('POST');
        //req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('SOAPAction', 'query');
        req.setHeader('Content-Length', String.valueOf(theXml.length()));
        req.setBody(theXml);
        System.Debug('### Request XML '+req.getBody());
        HttpResponse resp = conn.send(req);
        System.Debug('### Response XML '+resp.getBody());
        TpQueryResult tpRes;
        //
        // Ugly hack, but have to due to 2 callouts in this class.
        // Both of a different type. Hard to mock?
        // Yes, it could also be considered a design flaw. Connect has 
        // to be removed or also based on HTTP XML call, instead of wsdl2apex
        if(Test.isRunningTest()) {
            tpRes = new TpQueryResult();
        } else {
            tpRes = new TpQueryResult(resp.getBodyDocument(), this);
            Boolean doneFlag = tpRes.isDone;
            System.Debug('#### '+tpRes.isDone);
            if(!doneFlag) {
                while(!doneFlag) {
                    String newXml = TpXml.getQueryMoreXml(tpRes.queryLocator, this.sessionId);
                    HttpRequest req2 = new HttpRequest();
                    req2.setEndpoint(this.serverUrl);
                    req2.setMethod('POST');
                    //req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
                    req2.setHeader('Content-Type','text/xml;charset=UTF-8');
                    req2.setHeader('Accept-Encoding', 'gzip,deflate');
                    req2.setHeader('SOAPAction', 'queryMore');
                    req2.setHeader('Content-Length', String.valueOf(newXml.length()));
                    req2.setBody(newXml);
                    System.Debug('### Request XML '+req.getBody());
                    HttpResponse resp2 = conn.send(req2); 
                    tpRes.appendRows(resp2.getBodyDocument());
                    doneFlag = tpRes.isDone;
		            System.Debug('#### '+tpRes.isDone);
                }
            }
        }
        return tpRes;  		        
    }  
    
    //
    // Inserts records
    private TpInsertResponse insertLocal(List<TpSobject> tpSobjects) {
        System.Debug('### Local insert: '+tpSobjects);
        if(tpSobjects == null || tpSobjects.size() == 0) {
            throw new TpOrgException('Nothing to insert. Need TpSobjects');
        }   
        TpInsertResponse output = new TpInsertResponse();
        String objectName = tpSobjects[0].sobjectType.toLowerCase();
        if(this.orgSobjects.containsKey(objectName)) {
            
        	//
        	// Apparently this org knows the sobject
        	// Since we know it's local, we will still need to switch to Local Schema.SobjectType now
        	// because we cannot instantiate a concrete SObject() from a TpPartnerConnection.DescribeGlobalSObjectResult 
        	// instance. Through the API this would be possible, but I want to bypass the webservices completely for local orgs.
            Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();        	
            
            if(!gdMap.containsKey(objectName)) {
            	throw new TpOrgException('Error getting *'+objectName+'* token from local schema');    
            }
            Schema.SObjectType sot = gdMap.get(tpSobjects[0].sobjectType.toLowerCase());
     		Map<String, Schema.SObjectField> sofMap = sot.getDescribe().fields.getMap();
            List<SObject> sobjectsForInsert = new List<SObject>();
            List<ID> sourceIdList = new List<ID>();
            for(TpSobject tpso : tpSobjects) {
                
                //
                // Remove Id if present and add to cache
                if(tpso.fieldValueMap.containsKey('Id')) {
                    sourceIdList.add(tpso.fieldValueMap.get('Id'));
                    tpso.fieldValueMap.remove('Id'); 
                }
                if(tpso.fieldValueMap.containsKey('id')) {
                    sourceIdList.add(tpso.fieldValueMap.get('id'));
                    tpso.fieldValueMap.remove('id'); 
                }
                SObject so = tpso.getSobject(sot,sofMap,this.idCache);
			    sobjectsForInsert.add(so);    
            }
            
            //
            // Perform and handle the DML
            System.Debug('### Local insert '+sobjectsForInsert);
            Database.SaveResult[] results = Database.insert(sobjectsForInsert);
            System.Debug('### Local insert results '+results);
            output.numResults = results.size();
            Integer iterationCount = 0; 			
            for(Database.SaveResult sr : results) {
                TpSobjectResult objRes = new TpSobjectResult();
                if(sr.isSuccess()) {
                    objRes.newId = sr.getId();
                    objRes.isSuccess = sr.isSuccess();
                    objRes.sourceId = String.valueOf(sourceIdList[iterationCount]);
                } else {
           			objRes.sourceId = String.valueOf(sourceIdList[iterationCount]);
                    for(Database.Error err : sr.getErrors()) {
                      objRes.errors += err.getMessage()+'\n';    
                    }
                }
				System.Debug('### objRes '+objRes);
                //
                // Fill the cache with source -> target id's
                this.idCache.put(objRes.sourceId,objRes.newId);
                output.resultRows.add(objRes); 	
                
                iterationCount++;
                
            }
        } else {
            throw new TpOrgException('Unknown SObjectType');    
        }        
        return output;
    }      
    private TpInsertResponse insertRemote(List<TpSobject> tpSobjects) {
        System.Debug('### Remote insert: '+tpSobjects);
        if(tpSobjects == null || tpSobjects.size() == 0) {
            throw new TpOrgException('Nothing to insert. Need TpSobjects');
        }
        this.connect();
        
        //
        // Convert TpSobjects to XML
        String soXml = '';
        String sobjectName = tpSobjects[0].sobjectType.toLowerCase();
        Map<String, TpPartnerConnection.Field> fieldMap = this.getObjectFieldInfos(sObjectName);
        Set<String> excludeFields = new Set<String>{ 'id', 'type'}; // Type field (on account) is known issue, conflicts with Sobjecttype in the XML
        System.Debug('### tpSobjects '+tpSobjects);
        for(TpSobject tpso : tpSobjects) {
           soXml += tpso.getSobjectXml(fieldMap, this.idCache, excludeFields);
        }
        System.Debug('### SO XML '+soXml);
        String theXml = TpXml.getInsertXml(this.sessionId, soXml);
        System.debug('### Insert XML '+theXml);
        Http conn = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(this.serverUrl);
        req.setMethod('POST');
        req.setTimeout(10000);
        //req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('SOAPAction', 'create');
        //req.setHeader('Expect', '100-continue');
        req.setHeader('Content-Length', String.valueOf(theXml.length()));
        req.setBody(theXml);
        System.Debug('### Insert Request '+req.getBody()); 
        HttpResponse resp = conn.send(req);
        System.Debug('### Insert Response '+resp.getBody()); 
        TpInsertResponse tpInsResp = new TpInsertResponse(resp.getBodyDocument());
        Integer iterationCount = 0;
        for(TpSobjectResult s : tpInsResp.resultRows) {
            s.sourceId = String.valueOf(tpSobjects[iterationCount].fieldValueMap.get('id'));
            iterationCount++;
            if(!s.isSuccess) {
                //
                //  No need for caching failures
                continue;
            }                
            //
            // Fill cache map too, for usage on related sobjects
            this.idCache.put(s.sourceId,s.newId);
        }
        return tpInsResp;        

    }     
    //
    // Deletes records
    private TpDeleteResponse deleteLocal(List<TpSobject> tpSobjects) {
        System.Debug('### Local delete: '+tpSobjects);
        if(tpSobjects == null || tpSobjects.size() == 0) {
            throw new TpOrgException('Nothing to delete. Need TpSobjects');
        }   
        TpDeleteResponse output = new TpDeleteResponse();
        String objectName = tpSobjects[0].sobjectType.toLowerCase();
        if(this.orgSobjects.containsKey(objectName)) {
            
        	//
        	// Apparently this org knows the sobject
        	// Since we know it's local, we will still need to switch to Local Schema.SobjectType now
        	// because we cannot instantiate a concrete SObject() from a TpPartnerConnection.DescribeGlobalSObjectResult 
        	// instance. Through the API this would be possible, but I want to bypass the webservices completely for local orgs.
            Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();        	
            
            if(!gdMap.containsKey(objectName)) {
            	throw new TpOrgException('Error getting *'+objectName+'* token from local schema');    
            }
            Schema.SObjectType sot = gdMap.get(tpSobjects[0].sobjectType.toLowerCase());
     		Map<String, Schema.SObjectField> sofMap = sot.getDescribe().fields.getMap();
            List<SObject> sobjectsForDelete = new List<SObject>();
            List<ID> sourceIdList = new List<ID>();
            for(TpSobject tpso : tpSobjects) {
                SObject so = tpso.getSobject(sot,sofMap,this.idCache);
			    sobjectsForDelete.add(so);    
            }
            
            //
            // Perform and handle the DML
            System.Debug('### Local delete '+sobjectsForDelete);
            Database.DeleteResult[] results = Database.delete(sobjectsForDelete, false);
            System.Debug('### Local delete results '+results);
            output.numResults = results.size();
            for(Database.DeleteResult sr : results) {
                TpSobjectResult objRes = new TpSobjectResult();
                objRes.deletedId = sr.getId();
                if(sr.isSuccess()) {
                    objRes.isSuccess = sr.isSuccess();
                } else {
                    for(Database.Error err : sr.getErrors()) {
                      objRes.errors += err.getMessage()+'\n';    
                    }
                }
				System.Debug('### objRes '+objRes);
                output.resultRows.add(objRes);
            }
        } else {
            throw new TpOrgException('Unknown SObjectType');    
        }        
        return output;
    }      
    private TpDeleteResponse deleteRemote(List<TpSobject> tpSobjects) {
        System.Debug('### Remote delete: '+tpSobjects);
        if(tpSobjects == null || tpSobjects.size() == 0) {
            throw new TpOrgException('Nothing to delete. Need TpSobjects');
        }
        this.connect();
        
        //
        // Convert TpSobjects to XML
        String soXml = '';
		Set<String> idSet = new Set<String>();
        for(TpSobject s : tpSobjects) {
            //
            // Get all id's ... only thing we need for delete
            if(s.fieldValueMap.containsKey('id')) {
				idSet.add(s.fieldValueMap.get('id'));                
            }
            if(s.fieldValueMap.containsKey('Id')) {
				idSet.add(s.fieldValueMap.get('Id'));                
            }
        }
        String theXml = TpXml.getDeleteXml(this.sessionId, idSet);
        System.debug('### Delete XML '+theXml);
        Http conn = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(this.serverUrl);
        req.setMethod('POST');
        req.setTimeout(10000);
        //req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('Accept-Encoding', 'gzip,deflate');
        req.setHeader('SOAPAction', 'delete');
        //req.setHeader('Expect', '100-continue');
        req.setHeader('Content-Length', String.valueOf(theXml.length()));
        req.setBody(theXml);
        System.Debug('### Delete Request '+req.getBody()); 
        HttpResponse resp = conn.send(req);
        System.Debug('### Delete Response '+resp.getBody()); 
        TpDeleteResponse tpDelResp = new TpDeleteResponse(resp.getBodyDocument());
        return tpDelResp;        

    }     
    
    // -------------------------------------------------------
    // Describe info methods
    // ------------------------------------------------------- 
    //
    // Gets the information from a local org
    private List<TpPartnerConnection.DescribeGlobalSObjectResult> getLocalSobjectResults() {
        List<TpPartnerConnection.DescribeGlobalSObjectResult> outputList = new List<TpPartnerConnection.DescribeGlobalSObjectResult>();
        Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
        if(globalMap == null || globalMap.size() == 0) {
            throw new TpOrgException('Error while trying to fetch local SObject model');
        }

        for(Schema.SObjectType so : globalMap.values()) {
            Schema.DescribeSObjectResult descRes = so.getDescribe(); // Convert to generic form 
            TpPartnerConnection.DescribeGlobalSObjectResult apexObject = convertSchemaDescribeSObjectResultToApexObject(descRes);
            outputList.add(apexObject);
        }        
       
        return outputList;
    }
    
    //
    // Gets the information from a remote org
    private List<TpPartnerConnection.DescribeGlobalSObjectResult> getRemoteSobjectResults() {
        List<TpPartnerConnection.DescribeGlobalSObjectResult> outputList = new List<TpPartnerConnection.DescribeGlobalSObjectResult>();
        this.connect();    
        TpPartnerConnection.Soap conn = new TpPartnerConnection.Soap();                    
        conn.SessionHeader = new TpPartnerConnection.SessionHeader_element();
        conn.SessionHeader.sessionId = this.sessionId;
        conn.endpoint_x = this.serverUrl;
        TpPartnerConnection.DescribeGlobalResult describeResult;
        try {
            describeResult = conn.describeGlobal();
        }
        catch(Exception e) {
            this.calloutError = true;
            throw e;
        }
        outputList = describeResult.sobjects;
        return outputList;
    }

    private List<TpPartnerConnection.Field> getLocalFieldInfos(String sObjectName) {
        List<TpPartnerConnection.Field> output = new List<TpPartnerConnection.Field>();
    	Map<String, Schema.SObjectType> globalDesc = Schema.getGlobalDescribe();     
        if(globalDesc.containsKey(sObjectName)) {
            Schema.SObjectType sot = globalDesc.get(sObjectName); 
            Schema.DescribeSObjectResult descRes = sot.getDescribe();
            Map<String, Schema.SObjectField> allFields = descRes.fields.getMap();
            for(String fieldName : allFields.keySet()) {
                Schema.SObjectField field = allFields.get(fieldName);
                Schema.DescribeFieldResult fDesc = field.getDescribe();
                TpPartnerConnection.Field fieldInfo = convertSchemaDescribeFieldResultToApexObject(fDesc);
                output.add(fieldInfo);
            }  
        } else {
            throw new TpOrgException('SObject '+sObjectName+' not found on this org');
        }
        return output;
    }    
    private List<TpPartnerConnection.Field> getRemoteFieldInfos(String sObjectName) {
        List<TpPartnerConnection.Field> output = new List<TpPartnerConnection.Field>();
        this.connect();    
        TpPartnerConnection.Soap conn = new TpPartnerConnection.Soap();                    
        conn.SessionHeader = new TpPartnerConnection.SessionHeader_element();
        conn.SessionHeader.sessionId = this.sessionId;
        conn.endpoint_x = this.serverUrl;
        TpPartnerConnection.DescribeSObjectResult describeResult;
        try {
            describeResult = conn.describeSObject(sObjectName);
        }
        catch(Exception e) {
            this.calloutError = true;
            throw e;
        }
        if(describeResult <> null) {
        	output = describeResult.fields;
        } else {
            throw new TpOrgException('SObject '+sObjectName+' not found on this org');
        } 
        return output;
    }  
    //
    // Converts Schema results to apex classes. Not all information can be mapped onto the class
    private TpPartnerConnection.DescribeGlobalSObjectResult convertSchemaDescribeSObjectResultToApexObject(Schema.DescribeSObjectResult descRes) {
        TpPartnerConnection.DescribeGlobalSObjectResult outputObject = new TpPartnerConnection.DescribeGlobalSObjectResult();
        outputObject.createable = descRes.isCreateable();   
        outputObject.label = descRes.getLabel();
        outputObject.labelPlural = descRes.getLabelPlural();
        outputObject.custom = descRes.isCustom();
        outputObject.deprecatedAndHidden = descRes.isDeprecatedAndHidden();
        outputObject.queryable = descRes.isQueryable();
        outputObject.name = descRes.getName();
        outputObject.customSetting = descRes.isCustomSetting();
        outputObject.mergeable = descRes.isMergeable();
        outputObject.keyPrefix = descRes.getKeyPrefix();
        outputObject.feedEnabled = descRes.isFeedEnabled();
        outputObject.undeletable = descRes.isUndeletable();
        outputObject.updateable = descRes.isUpdateable();
        outputObject.searchable = descRes.isSearchable();
        return outputObject;
    }
    
    //
    // Converts Schema results to apex classes. Not all information can be mapped onto the class
    private TpPartnerConnection.Field convertSchemaDescribeFieldResultToApexObject(Schema.DescribeFieldResult descRes) {
        TpPartnerConnection.Field outputObject = new TpPartnerConnection.Field();
        outputObject.autoNumber = descRes.isAutoNumber();
        outputObject.createable = descRes.isCreateable();
		outputObject.byteLength = descRes.getByteLength();
        outputObject.calculated = descRes.isCalculated();
        outputObject.calculatedFormula = descRes.getCalculatedFormula();
        outputObject.cascadeDelete = descRes.isCascadeDelete();
        outputObject.caseSensitive = descRes.isCaseSensitive();
        //outputObject.controllerName= descRes.getController(); // Needs conversion
        outputObject.custom = descRes.isCustom() ;
        outputObject.defaultValueFormula = String.valueOf(descRes.getDefaultValue());
        outputObject.defaultedOnCreate = descRes.isDefaultedOnCreate();
        outputObject.dependentPicklist = descRes.isDependentPicklist();
        outputObject.deprecatedAndHidden = descRes.isDeprecatedAndHidden();
        outputObject.digits = descRes.getDigits();
       	//outputObject.displayLocationInDecimal = descRes.; // Does not exist on schema?
        outputObject.externalId = descRes.isExternalID();
        //outputObject.extraTypeInfo = descRes.;  // Does not exist on schema?
        outputObject.filterable = descRes.isFilterable();
        outputObject.groupable = descRes.isGroupable();
        //outputObject.highScaleNumber = descRes.; // Does not exist on schema?
        outputObject.htmlFormatted = descRes.isHtmlFormatted();
        outputObject.idLookup = descRes.isIdLookup();
        outputObject.inlineHelpText = descRes.getInlineHelpText();
        outputObject.label = descRes.getLabel();
        outputObject.length = descRes.getLength();
        //outputObject.mask = descRes.; // Does not exist on schema?
        //outputObject.maskType = descRes.; // Does not exist on schema?
        outputObject.name = descRes.getName();
        outputObject.nameField = descRes.isNameField();
        outputObject.namePointing = descRes.isNamePointing();
        outputObject.nillable = descRes.isNillable();
        outputObject.permissionable = descRes.isPermissionable();
        //outputObject.queryByDistance = descRes.; // Does not exist on schema?
        outputObject.referenceTargetField = descRes.getReferenceTargetField();
        //outputObject.referenceTo = descRes.getReferenceTo(); // Needs conversion
        outputObject.relationshipName = descRes.getRelationshipName();
        outputObject.relationshipOrder = descRes.getRelationshipOrder();
        outputObject.restrictedDelete = descRes.isRestrictedDelete();
        outputObject.restrictedPicklist = descRes.isRestrictedPicklist();
        outputObject.scale = descRes.getScale();
        outputObject.soapType = String.valueOf(descRes.getSOAPType());
        outputObject.sortable = descRes.isSortable();
        outputObject.type_x = String.valueOf(descRes.getType());
        outputObject.unique = descRes.isUnique();
        outputObject.updateable = descRes.isUpdateable();
        outputObject.writeRequiresMasterRead = descRes.isWriteRequiresMasterRead();
        outputObject.referenceTo = new List<String>();
        for(Schema.sObjectType sot : descRes.getReferenceTo()) {
        	outputObject.referenceTo.add(sot.getDescribe().getName());
        }
        //
        // Picklists handling
        Schema.DisplayType dispType = descRes.getType();
        if(dispType == Schema.DisplayType.PICKLIST || dispType == Schema.DisplayType.MULTIPICKLIST) {
			List<Schema.PicklistEntry> modelPlValues = descRes.getPicklistValues();   
			outputObject.picklistValues = new List<TpPartnerConnection.PicklistEntry>();
            for(Schema.PicklistEntry ple : modelPlValues) {
				TpPartnerConnection.PicklistEntry entry = new TpPartnerConnection.PicklistEntry();                    
                entry.active = ple.isActive();
                entry.defaultValue = ple.isDefaultValue();
                entry.label = ple.getLabel();
                entry.value = ple.getValue();
                outputObject.picklistValues.add(entry);    
            }    
        }
        return outputObject;
    }
    
   	//
    // Connect() will deal with session handling. 
    @TestVisible
    private TpPartnerConnection.LoginResult connect() {
        this.calloutError = false;
        if(String.isEmpty(this.userName) || String.isEmpty(this.password)) {
            throw new TpOrgException('No username / password to connect to remote org');    
        }
        if(this.isLocal) {
            throw new TpOrgException('Cannot connect to a local org!');    
        }
        String usePass = this.password;
        if(!String.isEmpty(this.securityToken)) {
            usePass += this.securityToken;
        }
        TpPartnerConnection.LoginResult output;
        if(this.sessionEnds <= System.now() || this.serverUrl == null || this.sessionId == null) {
            try {
                TpPartnerConnection.Soap conn = new TpPartnerConnection.Soap();
                conn.endpoint_x = this.loginEndpoint;
                output = conn.login(this.userName,usePass);
            }
            catch(Exception e) {
				this.calloutError = true;
                throw e;
            }
            if(output <> null && 
               output.sessionId <> null &&
               output.serverUrl <> null) {
                   this.serverUrl = output.serverUrl;
                   this.sessionId = output.sessionId;
                   this.orgName = output.userInfo.organizationName;
                   this.orgId = output.userInfo.organizationId;
                   this.loggedinUserName = output.userInfo.userName;
                   this.connectionTime = System.Now();
                   this.sessionEnds = System.Now().addSeconds(output.userInfo.sessionSecondsValid);
            } 
        }
        return output;
   	}
    private void initLocalOrg() {
        this.calloutError = false;
        this.isLocal = true;   
        this.userName = null;
        this.loggedinUserName = System.UserInfo.getUserName();
        this.password = null;
        this.securityToken = null;
        this.loginEndpoint = null;
        this.orgId = System.UserInfo.getOrganizationId();
        this.orgName = System.UserInfo.getOrganizationName();
        this.sessionEnds = null;
        this.connectionTime = null;
    }
    
   // -------------------------------------------------------
   // Custom exception
   // -------------------------------------------------------
   public class TpOrgException extends Exception {
        
   }
}