@isTest
private class Test_TpXml {
    private static testMethod void testXml() {
        //
        // Just coverage
        Test.startTest();
		String qXml = TpXml.getQueryXml('Select id from account', 'dummysessid');
		String insXml = TpXml.getInsertXml('dummysessionid', 'dummyxml');
        String delXml = TpXml.getDeleteXml('dummysessionid', new Set<String> { 'dummyid' });
    	Test.stopTest();        
    }
}