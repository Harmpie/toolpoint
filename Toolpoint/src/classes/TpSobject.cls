// -------------------------------------------------------
// A custom interpretation of SObjects. Used as abstraction layer between orgs
// -------------------------------------------------------
public class TpSobject {
    // nodeValue = nodeValue.unescapeXml();
   	// -------------------------------------------------------
   	// Class variables
   	// -------------------------------------------------------
	public String sobjectType {
        get; set;
    }
    //
    // Name -> Value
    public Map<String,String> fieldValueMap {
        get {
            if(fieldValueMap==null) {
                fieldValueMap = new Map<String,String>();
            }
            return fieldValueMap;
        }
        set;
   	}
    
   	// -------------------------------------------------------
   	// Constructors
   	// -------------------------------------------------------
   	public TpSobject(String soType) {
    	if(soType == null) {
            throw new TpSobjectException('No SObject type given!');
        }
        this.sobjectType = soType;
	}
   	public TpSobject() {
        this.sobjectType = 'Unset';
	}
    
   	// -------------------------------------------------------
   	// Public methods
   	// -------------------------------------------------------
    public SObject getSobject(Schema.SObjectType sod, Map<String, Schema.SObjectField> sofMap, Map<String, String> idCache){
        //
        // Must be for local usage, so we can use Schema
        if(sod == null) {
        	throw new TpSobjectException('Need a Schema.SObjectType!');    
        }
        SObject so = sod.newSobject();
        for(String fieldName : this.fieldValueMap.keySet()) {
        	this.setFieldValue(so, fieldName, this.fieldValueMap.get(fieldName), sofMap, idCache);
    	}   
        return so;
    }
    public String getSobjectXml(Map<String, TpPartnerConnection.Field> fieldMap, Map<String, String> cacheMap, Set<String> excludeFields){
        if(fieldMap == null) {
        	throw new TpSobjectException('Need a fieldMap!');    
        }
      	String sobjectsXml = '';
        sobjectsXml += '<urn:sObjects>\n';
        sobjectsXml += '<urn1:type>'+this.sobjectType+'</urn1:type>\n';
		for(String fieldName : fieldMap.keySet()) {
            TpPartnerConnection.Field fieldToken = fieldMap.get(fieldName);
            fieldName = fieldName.toLowerCase();
            //
            // Excludefields may contain 'id' for example. Or 'type' on account, which is known issue
            if(excludeFields.contains(fieldName)) {
                continue;
            }
            if(fieldToken.referenceTo <> null) {
                if(this.fieldValueMap.containsKey(fieldName) && !String.isEmpty(String.valueOf(this.fieldValueMap.get(fieldName))) && String.valueOf(this.fieldValueMap.get(fieldName)) <> 'null') {
                    if(cacheMap.containsKey(String.valueOf(this.fieldValueMap.get(fieldName)))) {
                        sobjectsXml += '<'+fieldName+'>'+cacheMap.get(String.valueOf(this.fieldValueMap.get(fieldName))).escapeXml()+'</'+fieldName+'>\n';     
                    }
                }
            } else {
                if(this.fieldValueMap.containsKey(fieldName) && !String.isEmpty(String.valueOf(this.fieldValueMap.get(fieldName))) && String.valueOf(this.fieldValueMap.get(fieldName)) <> 'null') {
                    sobjectsXml += '<'+fieldName+'>'+String.valueOf(this.fieldValueMap.get(fieldName)).escapeXml()+'</'+fieldName+'>\n';    
                }
            }
        }
		sobjectsXml += '</urn:sObjects>';
      	return sobjectsXml;
        
    }
    // -----------------------------------------------------
    // Private methods
    // -----------------------------------------------------
    private void setFieldValue(SObject s, String fieldName, String fieldValue, Map<String, Schema.SObjectField> sofMap, Map<String, String> idCache) {
        if(String.isEmpty(fieldName) || s == null || sofMap == null) {
            throw new TpSobjectException('Missing required arguments for setting Sobject value');
        }
        System.Debug('### SETTING NAME '+fieldName);
        System.Debug('### SETTING VALUE '+fieldValue);
        if(sofMap.containsKey(fieldName) && !String.isEmpty(fieldValue)) {
            Schema.DescribeFieldResult fieldDesc = sofMap.get(fieldName).getDescribe();
            if(fieldName.toLowerCase() <> 'id' && !fieldDesc.isCreateable()) {
                return;
            }
            Schema.DisplayType fieldType = fieldDesc.getType();
            //
            // Set correct type
            if(fieldType == Schema.DisplayType.ADDRESS) {
              // Unsupported    
            }
            if(fieldType == Schema.DisplayType.ANYTYPE) {
                //
                //  Try string
                s.put(fieldName, String.valueOf(fieldValue));             
            }
            if(fieldType == Schema.DisplayType.BASE64) {
                s.put(fieldName, Blob.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.BOOLEAN) {
                s.put(fieldName, Boolean.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.COMBOBOX) {
                s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.COMPLEXVALUE) {
        		// Formula  ? ... Readonly                
            }
            if(fieldType == Schema.DisplayType.CURRENCY) {
        		//s.put(fieldName, Decimal.valueOf(fieldValue));
        		s.put(fieldName, Decimal.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.DATACATEGORYGROUPREFERENCE) {
        		// Unsupported
            }
            if(fieldType == Schema.DisplayType.DATE) {
        		s.put(fieldName, Date.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.DATETIME) {
                try {
                  s.put(fieldName, DateTime.valueOf(fieldValue));
                    try {
                        s.put(fieldName, String.valueOf(fieldValue));
                    }
                    catch(Exception e) {
                        System.Debug('#### CRASH '+fieldValue);  
                    }
                }
                catch(Exception e) {
                  System.Debug('#### CRASH '+fieldValue);  
                }
            }
            if(fieldType == Schema.DisplayType.DOUBLE) {
                try {
                    s.put(fieldName, Double.valueOf(fieldValue));
                }
                catch(Exception e) {
                  System.Debug('#### CRASH '+fieldValue);  
                }
                //s.put(fieldName, fieldValue);
                //
            }
            if(fieldType == Schema.DisplayType.EMAIL) {
                s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.ENCRYPTEDSTRING) {
                s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.ID) {
                // readonly
                // 
                //
                // Needs additional handling
        		s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.INTEGER) {
                s.put(fieldName, Integer.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.LOCATION) {
        		s.put(fieldName, String.valueOf(fieldValue));                
            }
            if(fieldType == Schema.DisplayType.MULTIPICKLIST) {
               s.put(fieldName, String.valueOf(fieldValue));    
            }
            if(fieldType == Schema.DisplayType.PERCENT) {
              s.put(fieldName, Double.valueOf(fieldValue));    
            }
            if(fieldType == Schema.DisplayType.PHONE) {
                s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.PICKLIST) {
                s.put(fieldName, String.valueOf(fieldValue));
            }
            System.Debug('### Fieldname '+fieldName);
            if(fieldType == Schema.DisplayType.REFERENCE) {
                //
                // Needs additional handling
                // 
                System.Debug('### fieldName '+fieldName);
                System.Debug('### value '+idCache.get(String.valueOf(fieldValue)));
                System.Debug('### idCache '+idCache);
                
                if(idCache.containsKey(String.valueOf(fieldValue))) {
                   s.put(fieldName, idCache.get(String.valueOf(fieldValue))); 
                }
            }
            if(fieldType == Schema.DisplayType.SOBJECT) {
                //s.put(fieldName, String.valueOf(fieldValue));
            }
            if(fieldType == Schema.DisplayType.STRING) {
                s.put(fieldName, String.valueOf(fieldValue));
                
            }
            if(fieldType == Schema.DisplayType.TEXTAREA) {
                s.put(fieldName, String.valueOf(fieldValue));
                
            }
            if(fieldType == Schema.DisplayType.TIME) {
               // unsupported
                
            }
            if(fieldType == Schema.DisplayType.URL) {
        		s.put(fieldName, String.valueOf(fieldValue));
            }
        } // else, unknown field
    }
            
   	// -------------------------------------------------------
   	// Custom exception
   	// -------------------------------------------------------
   	public class TpSobjectException extends Exception {
        
   	}    
}