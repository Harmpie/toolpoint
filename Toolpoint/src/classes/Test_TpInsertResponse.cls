@isTest
private class Test_TpInsertResponse {
    //
    // Local insert responses are tested through TpOrg
    private static testMethod void testRemoteParser() {
        String xmlDummy = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Header><LimitInfoHeader><limitInfo><current>333</current><limit>15000</limit><type>API REQUESTS</type></limitInfo></LimitInfoHeader></soapenv:Header><soapenv:Body><createResponse><result><errors><message>FOUT</message><statusCode>FIELD_CUSTOM_VALIDATION_EXCEPTION</statusCode></errors><id xsi:nil="true"/><success>false</success></result><result><id>001b000000p2St9AAE</id><success>true</success></result></createResponse></soapenv:Body></soapenv:Envelope>';
		Dom.Document doc = new Dom.Document();
        doc.load(xmlDummy);
        Test.startTest();
        TpInsertResponse resp = new TpInsertResponse(doc);
        System.assertEquals(2,resp.numResults,'Result counter should be 2');
        System.assertEquals(2,resp.resultRows.size(),'Result rows should have 2 elements');
        Boolean foundErrors = false;
        for(TpSobjectResult res : resp.resultRows) {
            if(!String.isEmpty(res.errors)) {
                foundErrors = true;
            }
        }
        System.assertEquals(true,foundErrors,'Expected 1 row with errors. Did not find it');
        Test.stopTest();
    }
}