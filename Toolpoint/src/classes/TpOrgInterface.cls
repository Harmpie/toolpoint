global class TpOrgInterface {
    public TpOrg targetOrg { 
        get {
            if(targetOrg == null) {
                targetOrg = new TpOrg('','','','');
                //targetOrg = new TpOrg();
            }
            return targetOrg;
        }
        set; 
    }
    public TpOrg sourceOrg { 
        get {
            if(sourceOrg == null) {
                sourceOrg = new TpOrg('','','','');
            }
            return sourceOrg;
        }
        set; 
    }

    public TpOrgInterface() {
        
    }
    // ------------------------------------------------
    // Public methods
    // ------------------------------------------------
    public List<TpSObjectInterface> interfaces {
        get {
            if(interfaces == null) {
                interfaces = new List<TpSObjectInterface>();
                Integer counter = 0;
                for(String sobjectName : selectedSObjects) {
                    counter++;
                    TpSObjectInterface soitf = new TpSObjectInterface(this, sobjectName);
                    soitf.executionOrder = counter;
                    interfaces.add(soitf);
                }
            }
            interfaces.sort();
            return interfaces;
        }
        set;
    }

    public Boolean orgsDifferent {
        get {

            return !(this.targetOrg.orgSobjects.keySet() == this.sourceOrg.orgSobjects.keySet());
        }
        set;
    }
    public List<SelectOption> sObjectOptions {
        get {
            if(sObjectOptions == null) {
                sObjectOptions = new List<SelectOption>();
                if(this.targetOrg <> null && this.sourceOrg <> null) {
                    Set<String> workOnSet = new Set<String>();
                    Set<String> sourceSet = new Set<String>();
                    /*
                     *  Map<String, TpPartnerConnection.Field> getObjectFieldInfos(String sObjectName)
                     * Map<String,TpPartnerConnection.DescribeGlobalSObjectResult> orgSobjects
                     * */
                    for(String sobjectName : this.targetOrg.orgSobjects.keySet()) {
                        TpPartnerConnection.DescribeGlobalSObjectResult dr = this.targetOrg.orgSobjects.get(sobjectName);
                        if(dr.createable && dr.queryable && dr.updateable) {
                            workOnSet.add(sobjectName.toLowerCase());    
                        }
                    }
                    for(String sobjectName : this.sourceOrg.orgSobjects.keySet()) {
                        TpPartnerConnection.DescribeGlobalSObjectResult dr = this.sourceOrg.orgSobjects.get(sobjectName);
                        if(dr.createable && dr.queryable && dr.updateable) {
                            sourceSet.add(sobjectName.toLowerCase());    
                        }
                    }     
                    
                    //
                    // Only common sobjects will remain
                    Boolean changedSet = workOnSet.retainAll(sourceSet);
                    //this.orgsDifferent = changedSet;
                    //
                    // Only lists allow sorting. Transform set, we know now it has unique values
                    List<String> workOnList = new List<String>();
                    workOnList.addAll(workOnSet);
                    workOnList.sort();
                    
                    for(String sobjectName : workOnList) {
                        SelectOption so = new SelectOption(String.valueOf(sobjectName), String.valueOf(sobjectName));
                        sObjectOptions.add(so);
                    }
                }                
            }
            return sObjectOptions;
        }
        set;
    }    
    public List<String> selectedSObjects {
        get {
            if(selectedSObjects == null) {
                selectedSObjects = new List<String>();
            }
            return selectedSObjects;
        }
        set;
    }

    public Integer cacheSize {
        get {
           return this.targetOrg.idCache.size(); // Cache only relevant on target
        }
        //set;
    }    
    //
    // Determines common fields between target and source
    public Set<String> getCommonFieldList(String sobjectName) {
        Set<String> sourceFields = this.sourceOrg.getObjectFieldInfos(sobjectName).keySet();
        Set<String> targetFields = this.targetOrg.getObjectFieldInfos(sobjectName).keySet();
        sourceFields.retainAll(targetFields);
        return sourceFields;
    }    
    public Set<String> getWritableCommonFieldList(String sobjectName) {
        Set<String> sourceFields = this.sourceOrg.getObjectFieldInfos(sobjectName).keySet();
        Map<String, TpPartnerConnection.Field> fieldMap = this.targetOrg.getObjectFieldInfos(sobjectName);
        Set<String> targetFields = new Set<String>();
        // 
        // Strip all unwritable fields from the target
        for(String fieldName : fieldMap.keySet()) {
            TpPartnerConnection.Field f = fieldMap.get(fieldName);
            if(f.createable) {
                targetFields.add(fieldName);
            }
        }
        sourceFields.retainAll(targetFields);
        return sourceFields;
    }    
    
    public PageReference executeAll() {
        try {
            for(TpSobjectInterface tif : this.interfaces) {
                List<TpSobject> queryResults = this.sourceOrg.executeQuery(tif.query);
                tif.insertResults = this.targetOrg.executeInsert(queryResults); 
                
                ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Querying '+queryResults.size()+' '+tif.sobjectName+' rows from '+this.sourceOrg.orgName);
                ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Resulted in '+tif.insertResults.size()+' responses from '+this.targetOrg.orgName);
                
                ApexPages.addMessage(myMsg1);
                ApexPages.addMessage(myMsg2);
            }
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        
        return null;
    }
    public PageReference testAll() {
        try {
            for(TpSobjectInterface tif : this.interfaces) {
                List<TpSobject> results  = this.sourceOrg.executeQuery(tif.query);    
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Found '+results.size()+' '+tif.sObjectName+' rows');
                ApexPages.addMessage(myMsg);
            }
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        return null;
    }
    public PageReference clearCache() {
        try {
            this.sourceOrg.clearCache();
            this.targetOrg.clearCache();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Cache cleared');
            ApexPages.addMessage(myMsg);
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        return null;
    }
    public class TpInterfaceException extends Exception {
        
    }
    

}