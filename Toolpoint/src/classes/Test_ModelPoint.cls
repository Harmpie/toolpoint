@isTest
private class Test_ModelPoint {
    private static testMethod void testModelPoint() {
        Test.startTest();
        PageReference pageRef = Page.ModelPoint;
        Test.setCurrentPage(pageRef);
        ModelPointController controller = new ModelPointController();
        controller.theOrg = new TpOrg(); // Local 
		controller.theOrg.resetOrgConnection();

        System.assertEquals(true, controller.theOrg.isLocal,'Expected local org');
        System.assertEquals(null, controller.refreshSobs(),'Expected to stay in main screen');
		System.assertEquals(false, controller.showAllObjects,'All objects should be false');
		System.assertEquals(true, controller.writeableOnly,'Writeable only should be true');
		System.assertEquals(true, controller.showFieldInfo,'Field info should be true');
		System.assertEquals(0, controller.selectedSObjects.size(),'No objects should be selected');
		System.assert(controller.sobjectOptions.size() > 0,'Objects should be available');
        System.assertEquals(Page.ModelPoint.getUrl(), controller.getOutput().getUrl(),'Expected to end up in main screen');
        System.assertEquals(0, controller.objectFieldMap.size(),'Expect 0 elements in fieldmap');

        controller.selectedSObjects.add('account');  
        System.assertEquals(1, controller.objectFieldMap.size(),'Expect 1 element in fieldmap');
        System.assertEquals(Page.ModelPointOutput.getUrl(), controller.getOutput().getUrl(),'Expected to end up in export screen');
        controller.theOrg.isLocal = false;
        try {
            controller.theOrg.resetOrgConnection();
        }
        catch(Exception e) {
            System.Assert(true); // Cant connect to remote
        }
        System.assertEquals(null, controller.refreshSobs(),'Expected to stay in home screen');
        //
        // Untestable parts!
        controller.getCoverage();
        Test.stopTest();
        
    }
}