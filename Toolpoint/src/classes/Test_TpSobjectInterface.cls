@isTest
private class Test_TpSobjectInterface {
    private static testMethod void testSobjectItf() {
        Account a = new Account(Name='TEST APEX ACCOUNT');
        insert a;
        Contact c = new Contact(LastName='TEST APEX CONTACT',AccountId=a.Id);
        insert c;
        Test.startTest();
        TpOrgInterface itf = new TpOrgInterface();

        //
        // To avoid having to deal with callouts, use 2x local org here
        itf.sourceOrg.isLocal = true;
        itf.targetOrg.isLocal = true;
        itf.sourceOrg.resetOrgConnection();
        itf.targetOrg.resetOrgConnection();
		
		TpSobjectInterface soif1 = new TpSobjectInterface(itf, 'account');
        System.assert(soif1.query.toLowerCase().contains('from account'), 'Invalid account query');
        System.assert(soif1.sobjectName.toLowerCase()== 'account', 'Invalid sobject name expected account');
        System.assertEquals(0,soif1.insertResults.size(), 'Expected 0 results now');
        
        soif1.testItfQuery();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        
        TpSobjectInterface soif2 = new TpSobjectInterface(itf, 'contact');
        System.assert(soif2.query.toLowerCase().contains('from contact'), 'Invalid contact query');
        System.assert(soif2.sobjectName.toLowerCase() == 'contact', 'Invalid sobject name expected contact');
        System.assertEquals(0,soif2.insertResults.size(), 'Expected 0 results now');
        soif2.testItfQuery();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        
        soif1.executeItf();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Resulted in ') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        soif2.executeItf();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            System.assert(msg.getDetail().contains('Querying ') || msg.getDetail().contains('Resulted in ') || msg.getDetail().contains('Found '),'Unexpected PageMessage '+msg.getDetail());
        }
        System.assert(soif1.insertResults.size()>0,'Expected insert results on account');
        System.assert(soif2.insertResults.size()>0,'Expected insert results on contact');
        
        //
        // Test undo on soif2
        soif2.undoItf();
        for(Apexpages.Message msg : ApexPages.getMessages()) {
            //System.assert(msg.getDetail().contains('Undone '),'Unexpected PageMessage '+msg.getDetail());
        }
        System.assert(soif2.insertResults.size()==0,'Expected no insert results');
		
        //
        // Clear on account
	   	soif1.clearResults();
        System.assert(soif1.insertResults.size()==0,'Expected no insert results');
        
        //
        // Tests sorting
        soif1.executionOrder = 2;
        soif2.executionOrder = 1;
         
        List<TpSobjectInterface> soifList = new List<TpSobjectInterface> { soif1, soif2 };
		soifList.sort();
        //
        // Expect contact first now
        System.assertEquals('contact', soifList[0].sobjectName.toLowerCase(),'Sorting failed!');
        
        //
        // Attempt an export
        System.assert(soif1.openCsv().getUrl().contains('ACCOUNT') ,'Invalid CSV page'); // test account's name should be there!
        //
        // Attempt to generate testCode
        System.assert(soif1.generateApexTestData().getUrl().contains('public') ,'Invalid Apex Testdata page'); // class code starts with the word public
 
        Test.stopTest();
    }
}