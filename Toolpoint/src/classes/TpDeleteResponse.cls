public class TpDeleteResponse {
	//
	// TODO implementbulk handling
    private TpSobjectResult curRes;
    public Integer numResults {
        get {
            return resultRows.size();    
        }
        set;
    }
    
    public List<TpSobjectResult> resultRows {
        get {
            if(resultRows == null) {
                resultRows = new List<TpSobjectResult>();
            }
            return resultRows;
        }
        set;
    }
    //
    // Constructors
    public TpDeleteResponse() {
    }
    public TpDeleteResponse(Dom.Document xmlDoc) {
        parse(xmlDoc);
    }
    private void parse(Dom.Document xmlDoc) {
        Dom.XmlNode rootNode = xmlDoc.getRootElement();
        for(Dom.XMLNode child : rootNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'body') {
                handleBody(child);
            }
        }        
    }
    private void handleBody(Dom.XMLNode bodyNode) {
        System.Debug('### Body '+bodyNode);
        for(Dom.XMLNode child : bodyNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'deleteresponse') {
                handleQueryResponse(child);
            }
        }  
    }
    private void handleQueryResponse(Dom.XMLNode qrNode) {
        System.Debug('### QR '+qrNode);
        for(Dom.XMLNode child : qrNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'result') {
                this.curRes = new TpSobjectResult();
                handleResult(child);
            }
        }  
        
    }

    private void handleResult(Dom.XMLNode resultNode) {
        System.Debug('### Result '+resultNode);
        for(Dom.XMLNode child : resultNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'id') {
                this.curRes.deletedId = String.valueOf(child.getText());
            }
            if(child.getName().toLowerCase() == 'success') {
                this.curRes.isSuccess = Boolean.valueOf(child.getText());
            }
            if(child.getName().toLowerCase() == 'errors') {
                handleErrors(child);
            }
        } 
        this.resultRows.add(this.curRes);
    }
    private void handleErrors(Dom.XMLNode resultNode) {
        for(Dom.XMLNode child : resultNode.getChildElements()) {
            if(child.getName().toLowerCase() == 'message') {
                this.curRes.errors += String.valueOf(child.getText());
            }
            if(child.getName().toLowerCase() == 'statusCode') {
                this.curRes.errors += ' ['+String.valueOf(child.getText())+']';
            }       
        }
    }

}