public class TPExport {
    public static List<Report> getAllReports() {
        return [SELECT Id, Name, DeveloperName FROM Report];
    }
}