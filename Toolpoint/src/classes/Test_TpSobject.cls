@isTest
private class Test_TpSobject {
    private static testMethod void testTpSobject() {
        Test.startTest();
        try {
            TpSobject testFault = new TpSobject(null);
        }
        catch(Exception e) {
         	System.assert(true); // Should fail   
        }
        //
        // Get Sobject first
		TpSobject s = new TpSobject('Account');
        System.assertEquals('Account', s.sobjectType, 'TpSobject type should be Account');
		s.fieldValueMap.put('name','nameval');
        s.fieldValueMap.put('parentid','nix');
        // Add standard account fields of as many as possible types
        s.fieldValueMap.put('industry','chem');
        s.fieldValueMap.put('ownerid','falseid'); 
        s.fieldValueMap.put('fax','1234567'); 
        s.fieldValueMap.put('phone','7654321'); 
        s.fieldValueMap.put('BillingAddress',null); 
        s.fieldValueMap.put('website','http://apex.com'); 
        s.fieldValueMap.put('description','test desc'); 
        s.fieldValueMap.put('numberofemployees','2'); 
        s.fieldValueMap.put('annualrevenue','100'); 
        s.fieldValueMap.put('tickersymbol','x'); 
        s.fieldValueMap.put('createddate','yyy'); // readonly
        
        Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
        //fieldMap.put('account', Schema.Account.fields.Name);
        Schema.DescribeSObjectResult sor = Account.SObjectType.getDescribe();
        fieldMap = sor.fields.getMap();
        Map<String, String> idCache = new Map<String, String>();
        idCache.put('nix','EASYTEXTTORECOGNIZEINRESULTTHISISBOUNDTOBEUNIQUE');
        
        //
        // First fail
        try {
        	SObject so = s.getSobject(null, fieldMap, idCache);
        }
        catch(Exception e) {
         	System.assert(true); // Should fail   
        }
        //
        // Now success
        SObject so = s.getSobject(Schema.Account.sobjectType, fieldMap, idCache);
        
        //
        // Now setup XML
		Map<String, TpPartnerConnection.Field> fieldMap2 = new Map<String, TpPartnerConnection.Field>();
        TpPartnerConnection.Field f = new TpPartnerConnection.Field();
        f.name = 'name';
        f.referenceTo = null;
        TpPartnerConnection.Field f2 = new TpPartnerConnection.Field();
        f2.name = 'parentid';
        f2.referenceTo = new List<String> {'Account'};
        fieldMap2.put('name', f);
        fieldMap2.put('parentid', f2);
        
        //
        // First fail
        try {
        	String xml = s.getSobjectXml(null, idCache, new Set<String>());
        }
        catch(Exception e) {
         	System.assert(true); // Should fail   
        }
        //
        // Now success
        String xml = s.getSobjectXml(fieldMap2, idCache, new Set<String>());
        System.assert(xml.contains('EASYTEXTTORECOGNIZEINRESULTTHISISBOUNDTOBEUNIQUE'), 'Expected EASYTEXTTORECOGNIZEINRESULTTHISISBOUNDTOBEUNIQUE in the XML!');
        Test.stopTest();
    }
}