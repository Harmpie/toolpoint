public class TpSobjectResult {
    public String newId { get; set; }
    public String sourceId { get; set; }
    public String deletedId { get; set; }
    public Boolean isSuccess{ get; set; }
    public String errors { 
        get {
            if(errors == null) {
                errors ='';
            }    
            return errors;
        }
        set;
    }
}