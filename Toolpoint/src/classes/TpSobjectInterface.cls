global class TpSobjectInterface implements Comparable {
    public TpOrgInterface itf { get; set; }
    public String sobjectName { get; set; }
    public Integer executionOrder { get; set; }
    public List<TpSobjectResult> insertResults { 
        get {
            if(insertResults == null) {
                insertResults = new List<TpSobjectResult>();
            }
            return insertResults;            
        }
        set; 
    }
    public List<TpSobjectResult> deleteResults { 
        get {
            if(deleteResults == null) {
                deleteResults = new List<TpSobjectResult>();
            }
            return deleteResults;            
        }
        set; 
    }
	
    public TpSobjectInterface(TpOrgInterface itfInput, String sobjectNameInput) {
        this.itf = itfInput;
        this.sobjectName = sobjectNameInput;
    }
    public String query { 
        get {
            if(query == null) {
                query = 'SELECT Id '; 
                Set<String> fields = itf.getWritableCommonFieldList(this.sobjectName);
                for(String fieldName : fields) {
                    if(fieldName.toLowerCase() == 'id') {
                        continue;
                    }
                    query += ', '+fieldName;
                } 
                query += ' FROM '+this.sObjectName+' LIMIT 200';
            }
            return query;
        } 
        set; 
    }
    public PageReference testItfQuery() {
        try {
            System.assert(this.query <> null, 'No query!');
            List<TpSobject> results = this.itf.sourceOrg.executeQuery(this.query);
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Found '+results.size()+' '+this.sObjectName+' rows');
            ApexPages.addMessage(myMsg);
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }
    public PageReference executeItf() {
        try {
            List<TpSobject> queryResults = this.itf.sourceOrg.executeQuery(this.query);
            this.insertResults = this.itf.targetOrg.executeInsert(queryResults); 
            
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Querying '+queryResults.size()+' '+this.sobjectName+' rows from '+this.itf.sourceOrg.orgName);
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Resulted in '+this.insertResults.size()+' responses from '+this.itf.targetOrg.orgName);
            
            ApexPages.addMessage(myMsg1);
            ApexPages.addMessage(myMsg2);
            return null;
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }
    public PageReference undoItf() {
        try {
            System.assertNotEquals(null, this.insertResults, 'Cannot undo without results');
            List<TpSobject> toDel = new List<TpSobject>();
            for(TpSobjectResult res : this.insertResults) {
                TpSobject tps = new TpSobject(this.sobjectName);
                tps.fieldValueMap.put('id', res.newId);
                toDel.add(tps);
            }
            this.deleteResults = this.itf.targetOrg.executeDelete(toDel); 
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Undone '+this.deleteResults.size()+' '+this.sobjectName+' rows from '+this.itf.targetOrg.orgName);
            
            ApexPages.addMessage(myMsg1);
            this.insertResults = null;
            return null;
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }
    public PageReference clearResults() {
        this.insertResults = null;
        this.deleteResults = null;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Results cleared');
        ApexPages.addMessage(myMsg);
        
        return null;
    }
	//
	// Bonus features
    public PageReference openCsv() {
        try {
            System.assert(this.query <> null, 'No query!');
            List<TpSobject> results = this.itf.sourceOrg.executeQuery(this.query);
            if(results <> null && results.size() > 0) {
                //
                // Need full iteration for colheaders 
                // Each SO might have different values
                Set<String> colHeaders = new Set<String>(); 
                for(TpSobject so : results) {
                    for(String fieldName : so.fieldValueMap.keySet()) {
						colHeaders.add(fieldName);
                    }
                }
                Integer total = colHeaders.size();
                Integer counter = 0;
	            String csvContent = '';
                //
                // Set header cols
                for(String fName : colHeaders) {
                    counter++;
                    if(counter == total) {
                        csvContent += '"'+fName+'"';
                    } else {
                    	csvContent += '"'+fName+'",';    
                    }
                }
                csvContent += '\n';
				//
				// Data rows
                for(TpSobject so : results) {
                    counter = 1;
                    for(String fieldName : colHeaders) {
                        String useValue = (so.fieldValueMap.containsKey(fieldName)) ? so.fieldValueMap.get(fieldName).replaceAll('"','""') : '""';
                        if(counter == total) {
                            csvContent += '"'+useValue+'"';
                        } else {
                            csvContent += '"'+useValue+'",';
                        }
                        counter++;
                    }
                    csvContent += '\n';
                }
                PageReference r = Page.DataPointCsv;
                r.getParameters().put('csvText',csvContent);
                return r;
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Csv generation failed. No records');
                ApexPages.addMessage(myMsg);
            }
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }
    public PageReference generateApexTestData() {
        try {
            System.assert(this.query <> null, 'No query!');
            List<TpSobject> results = this.itf.sourceOrg.executeQuery(this.query);
            Map<String, TpPartnerConnection.Field> fieldMap = this.itf.sourceOrg.getObjectFieldInfos(this.sobjectName);
            
            if(results <> null && results.size() > 0) {

                Integer totalRows = results.size();
                String apexContent = '// Auto generated by ToolPoint\n';
                apexContent += 'public class TestData'+this.sobjectName+' {\n';
                apexContent += '	public static List<'+this.sobjectName+'> '+this.sobjectName+'TestData() {\n';
                apexContent += '		List<'+this.sobjectName+'> output = new List<'+this.sobjectName+'>();\n';
                Integer varCount = 1;
                for(TpSobject so : results) {
	                apexContent += '		'+this.sobjectName+' obj'+varCount+' = new '+this.sobjectName+'();\n';	
                    for(String fieldName : so.fieldValueMap.keySet()) {
                        if(fieldMap.containsKey(fieldName)) {
                            String apexAss = getApexAssignment('obj'+varCount, fieldName, so.fieldValueMap.get(fieldName),fieldMap.get(fieldName));
                            if(!String.isEmpty(apexAss)) {
                                apexContent += apexAss;
                            }
                        }						
                    }
                    apexContent += '		output.add(obj'+varCount+');\n';
                    varCount++;
                }            
                apexContent += '		return output;\n';
                apexContent += '	}\n';
                apexContent += '}\n';
                PageReference r = Page.DataPointApexTest;
                r.getParameters().put('apexCode',apexContent);
                return r;
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Csv generation failed. No records');
                ApexPages.addMessage(myMsg);
            }
        }   
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
    }

    //
    // Interface method for comparable
	global Integer compareTo(Object compareTo) {
     	TpSobjectInterface compareToitf = (TpSobjectInterface)compareTo;
        if (executionOrder == compareToitf.executionOrder) return 0;
        if (executionOrder > compareToitf.executionOrder) return 1;
        return -1;   
	}        

    private String getApexAssignment(String varName, String fieldName, String fieldValue, TpPartnerConnection.Field field) {
        if(String.isEmpty(fieldValue)) {
           return ''; 
        }
        String fieldType = field.type_x.toLowerCase();
        Set<String> castToString = new Set<String> {'string','picklist','textarea','reference','id','phone','fax','url'};
        Set<String> castToInt = new Set<String> {'integer'};
        Set<String> castToDate = new Set<String> {'date'};
        Set<String> castToDouble = new Set<String> {'double','currency'};
        Set<String> castToDateTime = new Set<String> {'double','currency'};
        Set<String> castToId = new Set<String> {'id', 'reference'};
        String output = '		'+varName+'.'+fieldName+' = ';
        if(castToString.contains(fieldType)) {
            fieldValue = fieldValue.replaceAll('\\n',''); 
            output += ' \''+fieldValue+'\';\n';
        } else if(castToDate.contains(fieldType)) {
            output += ' Date.valueOf(\''+fieldValue+'\');\n';                                   
        } else if(castToDouble.contains(fieldType)) {
            output += ' Double.valueOf(\''+fieldValue+'\');\n';
        } else if(castToDateTime.contains(fieldType)) {
            output +=' DateTime.valueOf(\''+fieldValue+'\');\n';
        } else if(castToInt.contains(fieldType)) {
            output +=' Integer.valueOf(\''+fieldValue+'\');\n';
        }else if(castToId.contains(fieldType)) {
            output +=' String.valueOf(\''+fieldValue+'\');\n';
            output += '//'+output;
        } else {
         	output += '';   
        }
        //output += fieldType+'\n';
        return output;
    }

}