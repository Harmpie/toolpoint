@isTest
private class Test_TpOrg {
    private static testMethod void testLocalTpOrg() {
        Account a1 = new Account(Name='Test Apex Account 1');
        Account a2 = new Account(Name='Test Apex Account 2');
        List<Account> accList = new List<Account>{ a1, a2 };
        insert accList;
        
        Test.startTest();
        TpOrg org = new TpOrg();
        System.assertEquals(true, org.isLocal, 'Expected a local org!');
        System.assertEquals(false, org.calloutError, 'Callout error should be false');
        System.assertEquals(0, org.idCache.size(), 'Expected empty id cache!');
        System.assertEquals(null, org.loginEndpoint, 'Expected empty loginEndpoint!');
        System.assertEquals(null, org.securityToken, 'Expected empty securityToken!');
        System.assertEquals(null, org.password, 'Expected empty password!');
        System.assertEquals(null, org.userName, 'Expected empty userName!');
        System.assertEquals(null, org.sessionId, 'Expected empty sessionId!');
        System.assertEquals(null, org.serverUrl, 'Expected empty serverUrl!');
        System.assertEquals(System.UserInfo.getUserName(), org.loggedinUserName, 'Expected '+System.UserInfo.getUserName()+' as loggedinUserName!');
        System.assertEquals(System.UserInfo.getOrganizationId(), org.orgId, 'Expected '+System.UserInfo.getOrganizationId()+' as orgId!');
        System.assertEquals(System.UserInfo.getOrganizationName(), org.orgName, 'Expected '+System.UserInfo.getOrganizationName()+' as orgName!');
		
        //
        // Execute a query locally
        List<TpSobject> results = org.executeQuery('SELECT Id, Name, BillingCity FROM Account');
        System.assertEquals(2, results.size(), 'Expected 2 query results!');
		
        //
		// Insert them and assert 4 rows 
        List<TpSobjectResult> insertResults = org.executeInsert(results);
        System.assert(insertResults <> null);
        System.assertEquals(2, insertResults.size(), 'Expected 2 results on insert!');
        System.assertEquals(2, org.idCache.size(), 'Expected 2 results in idCache!');
		
        //
        // Undo
        results = org.executeQuery('SELECT Id, Name, BillingCity FROM Account');
        System.assertEquals(4, results.size(), 'Expected 4 account rows');
        List<TpSobjectResult> deleteResults = org.executeDelete(results);
        System.assert(deleteResults <> null);
        System.assertEquals(4, deleteResults.size(), 'Expected 2 results on delete!');
        System.assertEquals(2, org.idCache.size(), 'Still expected 2 results in idCache!');
        
        Test.stopTest();
    }
    private static TpOrg connectedOrg {
        get {
            if(connectedOrg == null) {
				Test.setMock(WebServiceMock.class, new TpMockResponses.MockLoginResult());

                connectedOrg = new TpOrg('apex@test.user', 'secret', '', 'https://login.salesforce.com/services/Soap/u/33.0');
                connectedOrg.connect();
            }
            return connectedOrg;
        }
        set;
    }
    private static testMethod void testRemoteConnect() {
        Test.startTest();
        System.assertEquals(false, connectedOrg.isLocal, 'Expected a remote org!');
        System.assertEquals(false, connectedOrg.calloutError, 'Callout error should be false');
        System.assertEquals(0, connectedOrg.idCache.size(), 'Expected empty id cache!');
        System.assertEquals('https://login.salesforce.com/services/Soap/u/33.0', connectedOrg.loginEndpoint, 'Expected https://login.salesforce.com/services/Soap/u/33.0 as loginEndpoint!');
        System.assertEquals('', connectedOrg.securityToken, 'Expected empty string securityToken!');
        System.assertEquals('secret', connectedOrg.password, 'Expected secret as password!');
        System.assertEquals('apex@test.user', connectedOrg.userName, 'Expected apex@test.user as userName!');
        System.assertEquals('apextestsessionid1234', connectedOrg.sessionId, 'Expect sessionid apextestsessionid1234');
        System.assertEquals('https://apex.test.code/serverUrl', connectedOrg.serverUrl, 'Expected https://apex.test.code/serverUrl as serverUrl!');
        System.assertEquals('apex@test.user', connectedOrg.loggedinUserName, 'Expected apex@test.user as loggedinUserName!');
        System.assertEquals('apextestorgid', connectedOrg.orgId, 'Expected apextestorgid as orgId!');
        System.assertEquals('APEX TEST REMOTE ORG', connectedOrg.orgName, 'Expected APEX TEST REMOTE ORG as orgName!');
        Test.stopTest();
    }
   private static testMethod void testRemoteQuery() {
        Test.setMock(HttpCalloutMock.class, new TpMockResponses.MockQueryResult());
        //Test.setMock(WebServiceMock.class, new TpMockResponses.MultiRequestMock());
        Test.startTest();
        List<TpSobject> results = connectedOrg.executeQuery('SELECT Id, Name, BillingCity FROM Account');
       	System.assertEquals(0, results.size(),'Expected 0 results'); // Dummy response hacked into the TpOrg class has no rows
        Test.stopTest();
    }
    private static testMethod void testLocalFieldInfos() {
        Test.startTest();
        TpOrg localOrg = new TpOrg();
        System.assert(localOrg.getObjectFieldInfos('account') <> null);
        System.assert(localOrg.getObjectFieldInfos('account').size() > 0);
        Test.stopTest();
    }
    //
    // TODO IMPLEMENT correctly
    private static testMethod void testRemoteInsert() {
        Test.startTest();
        TpOrg theOrg = connectedOrg;
        List<TpSobject> tpSobjects = new List<TpSobject>();
        TpSobject tpso = new TpSobject('account');
        tpSobjects.add(tpso);
        theOrg.executeDelete(tpSobjects);
        Test.stopTest();
    }    
    private static testMethod void testRemoteDelete() {
        Test.startTest();
        TpOrg theOrg = connectedOrg;
        List<TpSobject> tpSobjects = new List<TpSobject>();
        TpSobject tpso = new TpSobject('account');
        tpSobjects.add(tpso);
        theOrg.executeInsert(tpSobjects);
        Test.stopTest();
    }    
    private static testMethod void testRemoteFieldInfos() {
        Test.startTest();
        TpOrg theOrg = connectedOrg;
        theOrg.getObjectFieldInfos('account');
        Test.stopTest();
    }    
    private static testMethod void testRemoteObjectInfos() {
        TpOrg theOrg = connectedOrg;
        Test.setMock(WebServiceMock.class, new TpMockResponses.MockDescribeGlobalResult());
        Test.startTest();
        System.assert(theOrg.orgSobjects <> null);
        System.assert(theOrg.orgSobjects.size() > 0);
        Test.stopTest();
    }
    private static testMethod void testLocalObjectInfos() {
        TpOrg theOrg = new TpOrg();
        Test.startTest();
        System.assert(theOrg.orgSobjects <> null);
        System.assert(theOrg.orgSobjects.size() > 0);
        Test.stopTest();
    }    
    /*
	https://login.salesforce.com/services/Soap/u/33.0
	https://test.salesforce.com/services/Soap/u/33.0      
	*/
}