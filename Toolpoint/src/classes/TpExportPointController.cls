public class TpExportPointController {
    public List<SelectOption> allReportOptions {
        get {
            if(allReportOptions == null) {
            	allReportOptions = new List<SelectOption>();
                for(Report r : this.allReportsMap.values()) {
                    SelectOption opt = new SelectOption(r.Id, r.Name);
                    allReportOptions.add(opt);
                }
            }
            return allReportOptions; 
        }
        set;
    }
    private Map<ID, Report> allReportsMap {
        get {
            if(allReportsMap == null) {
            	allReportsMap = new Map<ID, Report>();
                for(Report r : TPExport.getAllReports()) {
					allReportsMap.put(r.Id, r);
                }
            }
            return allReportsMap; 
        }
        set;
    }    
    public String selectedReportId {
        get; set;
    }
    private Report selectedReport {
        get {
            if(selectedReport == null) {
                if(selectedReportId <> null) {
					selectedReport = allReportsMap.get(selectedReportId);
                }                    
            }
            return selectedReport;
        }
        set;
    }
    private Reports.ReportResults selectedReportResults {
        get {
            if(selectedReportResults == null) {
                if(selectedReport <> null) {
					selectedReportResults = Reports.ReportManager.runReport(selectedReport.Id);
                }                    
            }
            return selectedReportResults;
        }
        set;
    }
    private Reports.ReportMetadata reportMd {
        get {
            if(reportMd == null) {
                if(this.selectedReportResults <> null) {
                	reportMd = this.selectedReportResults.getReportMetadata();        
                }
            }
            return reportMd;
     	}
        set;
    }
    public String reportType {
        get {
            if(reportType == null) {
                if(this.reportMd <> null) {
                	Reports.ReportFormat theFormat = this.reportMd.getReportFormat();  
                    if(theFormat == Reports.ReportFormat.MATRIX) {
                        reportType = 'Matrix';
                    } else if(theFormat == Reports.ReportFormat.SUMMARY) {
                        reportType = 'Summary';
                    } else if(theFormat == Reports.ReportFormat.TABULAR) {
                        reportType = 'Tabular';
                    } else if(theFormat == Reports.ReportFormat.MULTI_BLOCK) {
                        reportType = 'Multi';
                    } else {
                        reportType = 'Unknown';
                    }                
                }
            }
            return reportType;
     	}
        set;
    }
    public List<String> groups {
        get {
            groups = new List<String>();
            if(this.selectedReportResults <> null) {
                Reports.Dimension dim = this.selectedReportResults.getGroupingsDown();
                for(Reports.GroupingValue grp : dim.getGroupings()) {
                    groups.add((String)grp.getLabel());    
                }
            }	    
            return groups;
        }
        set;
    }  
}