@isTest
private class Test_TpQueryResult {
    private static testMethod void testRemoteQueryResponseParser() {
		String qrXml = TpXmlMock.getQueryResponseXml();
 		Dom.Document doc = new Dom.Document();
        doc.load(qrXml);
       	TpOrg dummy = new TpOrg();
        Test.startTest();
		TpQueryResult res = new TpQueryResult(doc, dummy);
        System.assertEquals(2,res.numRows,'Expected 2 results');
        System.assertEquals(2,res.sObjectRows.size(),'Expected 2 result rows');
        Test.stopTest();
    }
}